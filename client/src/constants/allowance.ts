import { parseUnits } from 'ethers/lib/utils';

// 20% of GLM total supply
export const MAX_GLM_ALLOWANCE = parseUnits('200000000');
