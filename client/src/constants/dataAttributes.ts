/**
 * IMPORTANT: these data attributes are used in CSS selectors too.
 * Whenever change of them is needed adjust CSS accordingly.
 */
export const IS_INITIAL_LOAD_DONE = 'data-is-initial-load-done';
