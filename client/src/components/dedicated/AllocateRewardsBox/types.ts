export default interface AllocateRewardsBoxProps {
  className?: string;
  isDisabled?: boolean;
  onUnlock?: () => void;
}
