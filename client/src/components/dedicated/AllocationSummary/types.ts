import { AllocationValues } from 'views/AllocationView/types';

export default interface AllocationSummaryProps {
  allocationValues: AllocationValues;
}
