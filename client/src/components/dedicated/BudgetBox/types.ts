export default interface BudgetBoxProps {
  className?: string;
  isCurrentlyLockedError?: boolean;
  isWalletBalanceError?: boolean;
}
