import { BigNumber } from 'ethers';

export default interface AllocationSummaryProjectProps {
  address: string;
  value: BigNumber;
}
