export default interface DonorsListProps {
  className?: string;
  dataTest?: string;
  proposalAddress: string;
}
