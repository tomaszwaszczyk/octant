import { HistoryItem } from 'hooks/queries/useHistory';

export default interface HistoryListProps {
  history: HistoryItem[] | undefined;
}
