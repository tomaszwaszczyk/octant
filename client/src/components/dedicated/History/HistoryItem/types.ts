import { HistoryItem } from 'hooks/queries/useHistory';

export default interface HistoryItemProps extends HistoryItem {}
