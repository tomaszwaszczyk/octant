import BoxRoundedProps from 'components/core/BoxRounded/types';

export default interface RewardsBoxProps {
  buttonProps?: BoxRoundedProps['buttonProps'];
  className?: string;
  isDisabled?: boolean;
  isGrey?: BoxRoundedProps['isGrey'];
}
