export type FormattedCryptoValue = {
  fullString: string;
  suffix: string;
  value: string;
};
