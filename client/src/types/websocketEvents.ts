// eslint-disable-next-line no-shadow
export enum WebsocketEmitEvent {
  allocate = 'allocate',
}

// eslint-disable-next-line no-shadow
export enum WebsocketListenEvent {
  allocationsSum = 'allocations_sum',
  proposalDonors = 'proposal_donors',
  threshold = 'threshold',
}
