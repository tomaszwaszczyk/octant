export type UseMediaQuery = { isDesktop: boolean; isMobile: boolean; isTablet: boolean };
