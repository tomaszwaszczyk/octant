import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';

import { GNT, TOKEN } from '../helpers/constants';

// This function needs to be declared this way, otherwise it's not understood by test runner.
// eslint-disable-next-line func-names
const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deploy } = hre.deployments;
  const { deployer } = await hre.getNamedAccounts();

  await deploy(GNT, {
    args: [],
    autoMine: true,
    contract: 'Token',
    from: deployer,
    log: true,
  });

  await deploy(TOKEN, {
    args: [],
    autoMine: true,
    contract: 'Token',
    from: deployer,
    log: true,
  });
};
export default func;
func.tags = ['token', 'local', 'test'];
