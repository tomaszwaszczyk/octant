import pytest

from app import database, exceptions
from app.controllers import rewards
from app.controllers.allocations import (
    get_all_by_user_and_epoch,
    get_all_by_proposal_and_epoch,
    get_sum_by_epoch,
    allocate,
    simulate_allocation,
)
from app.core.allocations import (
    deserialize_payload,
    AllocationRequest,
    Allocation,
)
from app.core.rewards.rewards import calculate_matched_rewards_threshold
from app.crypto.eip712 import sign, build_allocations_eip712_data
from app.extensions import db
from tests.conftest import (
    create_payload,
    MOCKED_PENDING_EPOCH_NO,
    MOCK_PROPOSALS,
    MOCK_EPOCHS,
    MOCK_GET_USER_BUDGET,
)


@pytest.fixture(autouse=True)
def before(
    app,
    proposal_accounts,
    patch_epochs,
    patch_proposals,
    patch_has_pending_epoch_snapshot,
    patch_user_budget,
):
    MOCK_PROPOSALS.get_proposal_addresses.return_value = [
        p.address for p in proposal_accounts[0:5]
    ]


def test_simulate_allocation_single_user(
    user_accounts, proposal_accounts, mock_pending_epoch_snapshot_db
):
    # Test data
    user1 = database.user.get_by_address(user_accounts[0].address)
    user1_allocations = [
        Allocation(proposal_accounts[0].address, 10 * 10**18),
        Allocation(proposal_accounts[1].address, 20 * 10**18),
        Allocation(proposal_accounts[2].address, 30 * 10**18),
    ]
    database.allocations.add_all(MOCKED_PENDING_EPOCH_NO, user1.id, user1_allocations)
    db.session.commit()

    payload = create_payload(proposal_accounts[0:2], [40 * 10**18, 50 * 10**18])
    proposal_rewards_before = rewards.get_proposals_rewards()

    assert len(proposal_rewards_before) == 3
    assert (
        proposal_rewards_before[0].address
        == "0x71bE63f3384f5fb98995898A86B02Fb2426c5788"
    )
    assert proposal_rewards_before[0].allocated == 20000000000000000000
    assert proposal_rewards_before[0].matched == 636044282804413348
    assert (
        proposal_rewards_before[1].address
        == "0xBcd4042DE499D14e55001CcbB24a551F3b954096"
    )
    assert proposal_rewards_before[1].allocated == 10000000000000000000
    assert proposal_rewards_before[1].matched == 318022141402206674
    assert (
        proposal_rewards_before[2].address
        == "0xFABB0ac9d68B0B445fB7357272Ff202C5651694a"
    )
    assert proposal_rewards_before[2].allocated == 30000000000000000000
    assert proposal_rewards_before[2].matched == 954066424206620023

    # Call simulate allocation method
    result = simulate_allocation(payload, user_accounts[0].address)

    assert len(result) == 2
    assert result[0].address == "0x71bE63f3384f5fb98995898A86B02Fb2426c5788"
    assert result[0].allocated == 50000000000000000000
    assert result[0].matched == 1060073804674022248
    assert result[1].address == "0xBcd4042DE499D14e55001CcbB24a551F3b954096"
    assert result[1].allocated == 40000000000000000000
    assert result[1].matched == 848059043739217798

    # Ensure changes made in the simulation are not saved to db
    proposal_rewards_after = rewards.get_proposals_rewards()
    assert proposal_rewards_before == proposal_rewards_after


def test_simulate_allocation_multiple_users(
    user_accounts, proposal_accounts, mock_pending_epoch_snapshot_db
):
    # Test data
    user1 = database.user.get_by_address(user_accounts[0].address)
    user2 = database.user.get_by_address(user_accounts[1].address)
    user1_allocations = [
        Allocation(proposal_accounts[0].address, 10 * 10**18),
        Allocation(proposal_accounts[1].address, 20 * 10**18),
        Allocation(proposal_accounts[2].address, 30 * 10**18),
    ]
    user2_allocations = [
        Allocation(proposal_accounts[1].address, 40 * 10**18),
        Allocation(proposal_accounts[2].address, 50 * 10**18),
    ]
    database.allocations.add_all(MOCKED_PENDING_EPOCH_NO, user1.id, user1_allocations)
    database.allocations.add_all(MOCKED_PENDING_EPOCH_NO, user2.id, user2_allocations)
    db.session.commit()

    payload = create_payload(proposal_accounts[0:2], [60 * 10**18, 70 * 10**18])
    proposal_rewards_before = rewards.get_proposals_rewards()

    assert len(proposal_rewards_before) == 3
    assert (
        proposal_rewards_before[0].address
        == "0x71bE63f3384f5fb98995898A86B02Fb2426c5788"
    )
    assert proposal_rewards_before[0].allocated == 60000000000000000000
    assert proposal_rewards_before[0].matched == 817771220748531448
    assert (
        proposal_rewards_before[1].address
        == "0xBcd4042DE499D14e55001CcbB24a551F3b954096"
    )
    assert proposal_rewards_before[1].allocated == 10000000000000000000
    assert proposal_rewards_before[1].matched == 0
    assert (
        proposal_rewards_before[2].address
        == "0xFABB0ac9d68B0B445fB7357272Ff202C5651694a"
    )
    assert proposal_rewards_before[2].allocated == 80000000000000000000
    assert proposal_rewards_before[2].matched == 1090361627664708598

    # Call simulate allocation method
    result = simulate_allocation(payload, user_accounts[0].address)

    assert len(result) == 3
    assert result[0].address == "0x71bE63f3384f5fb98995898A86B02Fb2426c5788"
    assert result[0].allocated == 110000000000000000000
    assert result[0].matched == 954066424206620023
    assert result[1].address == "0xBcd4042DE499D14e55001CcbB24a551F3b954096"
    assert result[1].allocated == 60000000000000000000
    assert result[1].matched == 520399867749065467
    assert result[2].address == "0xFABB0ac9d68B0B445fB7357272Ff202C5651694a"
    assert result[2].allocated == 50000000000000000000
    assert result[2].matched == 433666556457554556

    # Ensure changes made in the simulation are not saved to db
    proposal_rewards_after = rewards.get_proposals_rewards()
    assert proposal_rewards_before == proposal_rewards_after


def test_user_allocates_for_the_first_time(user_accounts, proposal_accounts):
    # Test data
    payload = create_payload(proposal_accounts[0:2], None)
    signature = sign(user_accounts[0], build_allocations_eip712_data(payload))

    # Call allocate method
    allocate(AllocationRequest(payload, signature, override_existing_allocations=True))

    # Check if allocations were created
    check_allocations(user_accounts[0].address, payload, 2)

    # Check if threshold is properly calculated
    check_allocation_threshold(payload)


def test_multiple_users_allocate_for_the_first_time(user_accounts, proposal_accounts):
    # Test data
    payload1 = create_payload(proposal_accounts[0:2], None)
    signature1 = sign(user_accounts[0], build_allocations_eip712_data(payload1))

    payload2 = create_payload(proposal_accounts[0:3], None)
    signature2 = sign(user_accounts[1], build_allocations_eip712_data(payload2))

    # Call allocate method for both users
    allocate(
        AllocationRequest(payload1, signature1, override_existing_allocations=True)
    )
    allocate(
        AllocationRequest(payload2, signature2, override_existing_allocations=True)
    )

    # Check if allocations were created for both users
    check_allocations(user_accounts[0].address, payload1, 2)
    check_allocations(user_accounts[1].address, payload2, 3)

    # Check if threshold is properly calculated
    check_allocation_threshold(payload1, payload2)


def test_allocate_updates_with_more_proposals(user_accounts, proposal_accounts):
    # Test data
    initial_payload = create_payload(proposal_accounts[0:2], None)
    initial_signature = sign(
        user_accounts[0], build_allocations_eip712_data(initial_payload)
    )

    # Call allocate method
    allocate(
        AllocationRequest(
            initial_payload, initial_signature, override_existing_allocations=True
        )
    )

    # Create a new payload with more proposals
    updated_payload = create_payload(proposal_accounts[0:3], None)
    updated_signature = sign(
        user_accounts[0], build_allocations_eip712_data(updated_payload)
    )

    # Call allocate method with updated_payload
    allocate(
        AllocationRequest(
            updated_payload, updated_signature, override_existing_allocations=True
        )
    )

    # Check if allocations were updated
    check_allocations(user_accounts[0].address, updated_payload, 3)

    # Check if threshold is properly calculated
    check_allocation_threshold(updated_payload)


def test_allocate_updates_with_less_proposals(user_accounts, proposal_accounts):
    # Test data
    initial_payload = create_payload(proposal_accounts[0:3], None)
    initial_signature = sign(
        user_accounts[0], build_allocations_eip712_data(initial_payload)
    )

    # Call allocate method
    allocate(
        AllocationRequest(
            initial_payload, initial_signature, override_existing_allocations=True
        )
    )

    # Create a new payload with fewer proposals
    updated_payload = create_payload(proposal_accounts[0:2], None)
    updated_signature = sign(
        user_accounts[0], build_allocations_eip712_data(updated_payload)
    )

    # Call allocate method with updated_payload
    allocate(
        AllocationRequest(
            updated_payload, updated_signature, override_existing_allocations=True
        )
    )

    # Check if allocations were updated
    check_allocations(user_accounts[0].address, updated_payload, 2)

    # Check if threshold is properly calculated
    check_allocation_threshold(updated_payload)


def test_multiple_users_change_their_allocations(user_accounts, proposal_accounts):
    # Create initial payloads and signatures for both users
    initial_payload1 = create_payload(proposal_accounts[0:2], None)
    initial_signature1 = sign(
        user_accounts[0], build_allocations_eip712_data(initial_payload1)
    )
    initial_payload2 = create_payload(proposal_accounts[0:3], None)
    initial_signature2 = sign(
        user_accounts[1], build_allocations_eip712_data(initial_payload2)
    )

    # Call allocate method with initial payloads for both users
    allocate(
        AllocationRequest(
            initial_payload1, initial_signature1, override_existing_allocations=True
        )
    )
    allocate(
        AllocationRequest(
            initial_payload2, initial_signature2, override_existing_allocations=True
        )
    )

    # Create updated payloads for both users
    updated_payload1 = create_payload(proposal_accounts[0:4], None)
    updated_signature1 = sign(
        user_accounts[0], build_allocations_eip712_data(updated_payload1)
    )
    updated_payload2 = create_payload(proposal_accounts[2:5], None)
    updated_signature2 = sign(
        user_accounts[1], build_allocations_eip712_data(updated_payload2)
    )

    # Call allocate method with updated payloads for both users
    allocate(
        AllocationRequest(
            updated_payload1, updated_signature1, override_existing_allocations=True
        )
    )
    allocate(
        AllocationRequest(
            updated_payload2, updated_signature2, override_existing_allocations=True
        )
    )

    # Check if allocations were updated for both users
    check_allocations(user_accounts[0].address, updated_payload1, 4)
    check_allocations(user_accounts[1].address, updated_payload2, 3)

    # Check if threshold is properly calculated
    check_allocation_threshold(updated_payload1, updated_payload2)


def test_allocation_validation_errors(proposal_accounts, user_accounts):
    # Test data
    payload = create_payload(proposal_accounts[0:3], None)
    signature = sign(user_accounts[0], build_allocations_eip712_data(payload))

    # Set invalid number of proposals on purpose (two proposals while three are needed)
    MOCK_PROPOSALS.get_proposal_addresses.return_value = [
        p.address for p in proposal_accounts[0:2]
    ]

    # Set invalid epoch on purpose (mimicking no pending epoch)
    MOCK_EPOCHS.get_pending_epoch.return_value = 0

    # Call allocate method, expect exception
    with pytest.raises(exceptions.NotInDecisionWindow):
        allocate(
            AllocationRequest(payload, signature, override_existing_allocations=True)
        )

    # Fix pending epoch
    MOCK_EPOCHS.get_pending_epoch.return_value = MOCKED_PENDING_EPOCH_NO

    # Call allocate method, expect invalid proposals
    with pytest.raises(exceptions.InvalidProposals):
        allocate(
            AllocationRequest(payload, signature, override_existing_allocations=True)
        )

    # Fix missing proposals
    MOCK_PROPOSALS.get_proposal_addresses.return_value = [
        p.address for p in proposal_accounts[0:3]
    ]

    # Expect no validation errors at this point
    allocate(AllocationRequest(payload, signature, override_existing_allocations=True))


def test_project_allocates_funds_to_itself(proposal_accounts, user_accounts):
    # Test data
    payload = create_payload(proposal_accounts[0:3], None)
    signature = sign(proposal_accounts[0], build_allocations_eip712_data(payload))

    with pytest.raises(exceptions.ProposalAllocateToItself):
        allocate(
            AllocationRequest(payload, signature, override_existing_allocations=True)
        )


def test_get_by_user_and_epoch(mock_allocations_db, user_accounts, proposal_accounts):
    result = get_all_by_user_and_epoch(
        user_accounts[0].address, MOCKED_PENDING_EPOCH_NO
    )

    assert len(result) == 3
    assert result[0].address == proposal_accounts[0].address
    assert result[0].amount == str(10 * 10**18)
    assert result[1].address == proposal_accounts[1].address
    assert result[1].amount == str(5 * 10**18)
    assert result[2].address == proposal_accounts[2].address
    assert result[2].amount == str(300 * 10**18)


def test_get_by_proposal_and_epoch(
    mock_allocations_db, user_accounts, proposal_accounts
):
    result = get_all_by_proposal_and_epoch(
        proposal_accounts[1].address, MOCKED_PENDING_EPOCH_NO
    )

    assert len(result) == 2
    assert result[0].address == user_accounts[0].address
    assert result[0].amount == str(5 * 10**18)
    assert result[1].address == user_accounts[1].address
    assert result[1].amount == str(1050 * 10**18)


def test_get_sum_by_epoch(mock_allocations_db, user_accounts, proposal_accounts):
    result = get_sum_by_epoch(MOCKED_PENDING_EPOCH_NO)
    assert result == 1865 * 10**18


def test_user_exceeded_rewards_budget_in_allocations(
    app, proposal_accounts, user_accounts
):
    # Set some reasonable user rewards budget
    MOCK_GET_USER_BUDGET.return_value = 100 * 10**18

    # First payload sums up to 110 eth (budget is set to 100)
    payload = create_payload(
        proposal_accounts[0:3], [10 * 10**18, 50 * 10**18, 50 * 10**18]
    )
    signature = sign(user_accounts[0], build_allocations_eip712_data(payload))

    with pytest.raises(exceptions.RewardsBudgetExceeded):
        allocate(
            AllocationRequest(payload, signature, override_existing_allocations=True)
        )

    # Lower it to 100 total (should pass)
    payload = create_payload(
        proposal_accounts[0:3], [10 * 10**18, 40 * 10**18, 50 * 10**18]
    )
    signature = sign(user_accounts[0], build_allocations_eip712_data(payload))
    allocate(AllocationRequest(payload, signature, override_existing_allocations=True))


def check_allocations(user_address, expected_payload, expected_count):
    epoch = MOCKED_PENDING_EPOCH_NO
    expected_allocations = deserialize_payload(expected_payload)
    user = database.user.get_by_address(user_address)
    assert user is not None

    db_allocations = database.allocations.get_all_by_epoch_and_user_id(epoch, user.id)
    assert len(db_allocations) == expected_count

    for db_allocation, expected_allocation in zip(db_allocations, expected_allocations):
        assert db_allocation.epoch == epoch
        assert db_allocation.user_id == user.id
        assert db_allocation.user is not None
        assert db_allocation.proposal_address == expected_allocation.proposal_address
        assert int(db_allocation.amount) == expected_allocation.amount


def check_allocation_threshold(*payloads):
    epoch = MOCKED_PENDING_EPOCH_NO
    projects_no = 5
    expected = [deserialize_payload(payload) for payload in payloads]

    db_allocations = database.allocations.get_all_by_epoch(epoch)

    total_allocations = sum([int(allocation.amount) for allocation in db_allocations])
    total_payload_allocations = sum(
        [allocation.amount for allocations in expected for allocation in allocations]
    )

    assert total_allocations == total_payload_allocations

    expected_threshold = int(total_allocations / (projects_no * 2))

    assert expected_threshold == calculate_matched_rewards_threshold(
        total_allocations, projects_no
    )
